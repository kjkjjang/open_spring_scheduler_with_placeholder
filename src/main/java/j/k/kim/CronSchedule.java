package j.k.kim;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/*
 * 	cron expression
초(Seconds)
분(Minutes)
시(Hours)
일(Day-of-Month)
월(Months)
요일(Days-of-Week)
 */
@Component
public class CronSchedule {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

	
	@Scheduled(cron="0 0 8 * * *")
	public void cronAt8Clock() {
		LocalDateTime dateTime = LocalDateTime.now();
		logger.info("cronAt8Clock start[" + dateTime.format(dtf) + "]");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.info("cronAt8Clock end[" + dateTime.format(dtf) + "]");
	}
	
	@Scheduled(fixedRate=5000)
	public void cronFiledRate() {
		LocalDateTime dateTime = LocalDateTime.now();
		logger.info("cronFiledRate start[" + dateTime.format(dtf) + "]");
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.info("cronFiledRate end[" + dateTime.format(dtf) + "]");
	}
	
	@Value(value="${enabled_file_send}")
	private boolean fileSendEnabled;
	
	@Scheduled(cron="${expression_file_send}")
	public void fileSendScheduler() {
		LocalDateTime dateTime = LocalDateTime.now();
		logger.info("fileSendScheduler start[" + dateTime.format(dtf) + "][" + fileSendEnabled + "]");
	}
	
}
