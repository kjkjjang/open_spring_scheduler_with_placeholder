package j.k.kim.config;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.jdbc.core.JdbcTemplate;

import j.k.kim.CronItem;

public class DbPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private List<CronItem> loadList;

	public DbPlaceholderConfigurer(JdbcTemplate jdbcTemplate) {
		loadList = jdbcTemplate.query("select cron_name, cron_value, cron_enabled from cron_data", (rs, rowNum) -> {
			CronItem item = new CronItem();
			item.setCron_name(rs.getString("cron_name") );
			item.setCron_value(rs.getString("cron_value") );
			item.setCron_enabled(rs.getBoolean("cron_enabled"));
			return item;
		});
	}

	@Override
	protected void loadProperties(Properties props) throws IOException {
		super.loadProperties(props);
		loadList.stream().forEach(item -> {
			props.setProperty("expression_" + item.getCron_name(), item.getCron_value());
			props.setProperty("enabled_" + item.getCron_name(), String.valueOf(item.isCron_enabled() ) );
		});
		logger.debug("properties setting end");

	}

}
