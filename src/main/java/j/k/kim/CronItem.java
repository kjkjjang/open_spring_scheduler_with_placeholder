package j.k.kim;

public class CronItem {

	private String cron_name;
	private String cron_value;
	private boolean cron_enabled;
	private String note;

	public String getCron_name() {
		return cron_name;
	}

	public void setCron_name(String cron_name) {
		this.cron_name = cron_name;
	}

	public String getCron_value() {
		return cron_value;
	}

	public void setCron_value(String cron_value) {
		this.cron_value = cron_value;
	}

	public boolean isCron_enabled() {
		return cron_enabled;
	}

	public void setCron_enabled(boolean cron_enabled) {
		this.cron_enabled = cron_enabled;
	}

}
