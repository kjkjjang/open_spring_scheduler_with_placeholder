CREATE TABLE CRON_DATA
(
	cron_name		VARCHAR(30) PRIMARY KEY,
	cron_value		VARCHAR(50) NOT NULL,
	cron_enabled	BOOLEAN ,
	note			VARCHAR(100)
);
